package com.miki.utils;

import com.miki.constants.Browsers;
import com.miki.mikinet.BookingDetails;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by asharma on 12/06/14.
 */
public class BrowserFactory {


    private static final Logger LOGGER = Logger.getLogger(BrowserFactory.class.getName());

    // browser to launch
    private static final String BROWSER_PROP_KEY = "CHROME";


    /**
     * creates the browser driver specified in the system property "browser"
     * if no property is set then a firefox browser driver is created.
     * The allow properties are firefox, safari and chrome
     * e.g to run with safari, pass in the option -Dbrowser=safari at runtime
     *
     * @return WebDriver
     */
    public static WebDriver getBrowser() {
        Browsers browser;
        WebDriver driver;

        if (System.getProperty(BROWSER_PROP_KEY) == null) {

            browser = Browsers.FIREFOX;
//            browser = Browsers.CHROME;
        } else {
            browser = Browsers.browserForName(System.getProperty(BROWSER_PROP_KEY));
        }
        switch (browser) {
            case CHROME:
                System.out.println("Trying to open chrome driver");
                driver = createChromeDriver();

                break;
            case SAFARI:
                driver = createSafariDriver();
                break;
            case FIREFOX:
                System.out.println("Trying to open FIREFOX driver");
                driver = new FirefoxDriver();
                break;
            default:
//                System.out.println(" coming to default browser");
//                driver = createFirefoxDriver(getFirefoxProfile());

//                changed by Apara
                LOGGER.info("creating new driver");
                driver = new FirefoxDriver();
                LOGGER.info("New Firefox driver created");
                break;
        }
        addAllBrowserSetup(driver);
        return driver;
    }

    private static WebDriver createSafariDriver() {
        return new SafariDriver();
    }

    private static WebDriver createChromeDriver() {
        String chromeDriverName;
        if ((System.getProperty("os.name").toLowerCase().indexOf("win") >= 0)) {
            chromeDriverName = "chromedriver.exe";
        } else {
            chromeDriverName = "chromedriver";
        }
        StringBuffer sb = new StringBuffer();
        sb.append(System.getProperty("user.dir"))
                .append(File.separatorChar)
                .append("src")
                .append(File.separatorChar)
                .append("main")
                .append(File.separatorChar)
                .append("resources")
                .append(File.separatorChar)
                .append(chromeDriverName);

        System.setProperty("webdriver.chrome.driver", sb.toString());
        return new ChromeDriver();
    }

//    private static WebDriver createFirefoxDriver(FirefoxProfile firefoxProfile) {
//        System.out.println("Firefox profile");
//        System.out.println(firefoxProfile.getClass());
//        return new FirefoxDriver(firefoxProfile);
//    }
//
//    private static FirefoxProfile getFirefoxProfile() {
//        FirefoxProfile firefoxProfile = new FirefoxProfile();
//        try {
//            firefoxProfile.addExtension(FileUtils.getFile("firebug/firebug-1.9.2.xpi"));
//        } catch (IOException e) {
//
//            System.out.println("exception raised here ");
//            e.printStackTrace();
//        } catch (URISyntaxException e) {
//            System.out.println("exception raised here YET AGAIN");
//            e.printStackTrace();
//        }
//
////        See http://getfirebug.com/wiki/index.php/Firebug_Preferences
//        firefoxProfile.setPreference("extensions.firebug.currentVersion", "1.9.2");  // Avoid startup screen
//        firefoxProfile.setPreference("extensions.firebug.script.enableSites", true);
//        firefoxProfile.setPreference("extensions.firebug.console.enableSites", true);
//        firefoxProfile.setPreference("extensions.firebug.allPagesActivation", true);
//        firefoxProfile.setPreference("extensions.firebug.delayLoad", false);
//        return firefoxProfile;
//    }

    private static void addAllBrowserSetup(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);
        driver.manage().window().setPosition(new Point(0, 0));
        java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        org.openqa.selenium.Dimension dim = new org.openqa.selenium.Dimension((int) screenSize.getWidth(), (int) screenSize.getHeight());
        driver.manage().window().setSize(dim);
    }


    public static ArrayList getBookingList() {
        LOGGER.info("Creating a new Booking ArrayList");
        ArrayList<BookingDetails> bookinglist;
        bookinglist = new ArrayList<BookingDetails>();
        return bookinglist;
    }
}
