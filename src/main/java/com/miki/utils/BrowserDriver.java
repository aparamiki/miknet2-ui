package com.miki.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by asharma on 12/06/14.
 */
public class BrowserDriver {

    private static final Logger LOGGER = Logger.getLogger(BrowserDriver.class.getName());
    // webDriver generic webdriver .. implementation - chrome, firefox driver
//    private static WebDriver mDriver;

    private static WebDriver mDriver;

    // apara - 23/07/2014
    private static ArrayList bookingDetailList;

    public synchronized static WebDriver getCurrentDriver() {
        if (mDriver == null) {

            try {
                mDriver = BrowserFactory.getBrowser();
            } catch (UnreachableBrowserException e) {
                mDriver = BrowserFactory.getBrowser();
            } catch (WebDriverException e) {
                mDriver = BrowserFactory.getBrowser();
            } finally {
                Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
            }
        }
        return mDriver;
    }

    public static void close() {
        try {
            getCurrentDriver().quit();
            mDriver = null;
            LOGGER.info("closing the browser");
        } catch (UnreachableBrowserException e) {
            LOGGER.info("cannot close browser: unreachable browser");
        }
    }

    private static class BrowserCleanup implements Runnable {
        public void run() {
            close();
        }
    }


    public static void loadPage(String url) {
        getCurrentDriver();
        LOGGER.info("Directing browser to:" + url);

        getCurrentDriver().get(url);
    }


    public static String getPageTitle() {
        return getCurrentDriver().getTitle();
    }

    public static void reopenAndLoadPage(String url) {
        mDriver = null;
        getCurrentDriver();
        loadPage(url);
    }

    public static WebElement waitForElement(WebElement elementToWaitFor) {
        return waitForElement(elementToWaitFor, null);
    }

    public static WebElement waitForElement(WebElement elementToWaitFor, Integer waitTimeInSeconds) {
        if (waitTimeInSeconds == null) {
            waitTimeInSeconds = 10;
        }

        WebDriverWait wait = new WebDriverWait(getCurrentDriver(), waitTimeInSeconds);
        return wait.until(ExpectedConditions.visibilityOf(elementToWaitFor));
    }

    public static WebElement getParent(WebElement element) {
        return element.findElement(By.xpath(".."));
    }

    public static List<WebElement> getDropDownOptions(WebElement webElement) {
        Select select = new Select(webElement);
        return select.getOptions();
    }

    public static WebElement getDropDownOption(WebElement webElement, String value) {
        WebElement option = null;
        List<WebElement> options = getDropDownOptions(webElement);
        for (WebElement element : options) {
            if (element.getAttribute("value").equalsIgnoreCase(value)) {
                option = element;
                break;
            }
        }
        return option;
    }


    public static WebElement getElementByIdentifier(String eidentifier, String type) {

        if (type == "CSS") {
            return mDriver.findElement(By.cssSelector(eidentifier));
        } else if (type == "ID") {
            return mDriver.findElement(By.id((eidentifier)));
        } else
            return null;

    }


    public static void performMouseOver_and_moveToNewElement(WebElement element, String newElementCSS, String Identifier) {
        Actions action = new Actions(mDriver);
        action.moveToElement(element).moveToElement(getElementByIdentifier(newElementCSS, Identifier)).click().perform();

    }


    public static void selectDropDownValue(WebElement ele, String val) {
        try {
            LOGGER.info("Entering the drop down value");
            LOGGER.info(ele.toString() + val.toString());
            Select clickThis = new Select(ele);
            clickThis.selectByVisibleText(val);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("selection from drop down failed");
        }
    }

    /**
     * Return 1st select element
     *
     * @param table
     * @return WebElement
     * Apara
     */

    public static WebElement readSelectElementFromTable(WebElement table) {
        List<WebElement> rows = table.findElements(By.tagName("select"));
//        java.util.Iterator<WebElement> i = rows.iterator();
//        while (i.hasNext()) {
//            WebElement row = i.next();
//        System.out.println("*******************" +  row.getTagName());
//        }
        return rows.get(0);
    }

    // candidate for generalisation
    public static WebElement readElementFromTable(WebElement table) {
        List<WebElement> rows = table.findElements(By.tagName("td"));
//        java.util.Iterator<WebElement> i = rows.iterator();
//        while (i.hasNext()) {
//            WebElement row = i.next();
//        System.out.println("*******************" +  row.getTagName());
//        }
        return rows.get(0);
    }


    //23/07/2014 Apara
    public synchronized static ArrayList getBookingArrayList() {
        if (bookingDetailList == null) {
            bookingDetailList = BrowserFactory.getBookingList();
        }
        return bookingDetailList;
    }
}
