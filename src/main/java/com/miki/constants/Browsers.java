package com.miki.constants;

/**
 * Created by asharma on 12/06/14.
 */
public enum Browsers {

    FIREFOX,
    CHROME,
    SAFARI;

    public static Browsers browserForName(String browser) throws IllegalArgumentException {
        for (Browsers b : values()) {
            if (b.toString().equalsIgnoreCase(browser)) {
                return b;
            }
        }
        throw browserNotFound(browser);
    }

    private static IllegalArgumentException browserNotFound(String outcome) {
        return new IllegalArgumentException(("Invalid browser [" + outcome + "]"));
    }
}
