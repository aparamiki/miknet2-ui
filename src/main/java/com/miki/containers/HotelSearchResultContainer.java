package com.miki.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 02/07/14.
 */
public class HotelSearchResultContainer {

    public static String HOTELSEARCHRESULTPAGETITLE = "mikiNet - Hotel search";


    @FindBy(how = How.CSS, using = "input.primary")
    public WebElement bookHotel1;


    @FindBy(how = How.XPATH, using = "//input[@value='Book'])[2]")
    public WebElement bookHotel2;

    @FindBy(how = How.CSS, using = "strong")
//    @CacheLookup
    public WebElement NumberOfHotelsReturnedText;

    @FindBy(how = How.CSS, using = "a.backward")
//    @CacheLookup
    public WebElement backButton;


}


