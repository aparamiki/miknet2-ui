package com.miki.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 12/06/14.
 */
public class LoginPageContainer {


    private String validationContainerCSS = "div.dijitValidationContainer";

//    public static String LoginPageTitle = "mikiNet - Home";


    @FindBy(how = How.CSS, using = "h2.secureLogin")
    public WebElement logInForm;



    @FindBy(how = How.ID, using = "clientCode")
    public WebElement clientCode;


    @FindBy(how = How.ID, using = "username")
    public WebElement usernameInput;


    @FindBy(how = How.ID, using = "password")
    public WebElement passwordInput;


    @FindBy(how = How.CSS, using = "input.primary")
    public WebElement submitButton;


}
