package com.miki.containers.bookingSearch;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Confirmation Page showing booking cancelled status
 * Created by asharma on 20/02/15.
 */
public class CancelBookingConfirmed_Containter {


    @FindBy(how = How.CSS, using = "h1")
    @CacheLookup
    public WebElement cancellationConfirmedHeader;

    @FindBy(how = How.LINK_TEXT, using = "Logout")
    @CacheLookup
    public WebElement logout;
}
