package com.miki.containers.bookingSearch;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 13/02/15.
 */
public class ModifyView_2ndPageContainer {

    //    @FindBy(how = How.CSS, using = "td.multiplePax > table.0.1")
    @FindBy(how = How.NAME, using = "modifiedDetail.roomsList[0].roomNames[0].title")
    public  WebElement paxtitle1;


    //    @FindBy(how = How.CSS, using = "td.multiplePax > table.0.2")
    @FindBy(how = How.NAME, using = "modifiedDetail.roomsList[0].roomNames[0].firstName")
    public  WebElement paxfirstname1;


    //    @FindBy(how = How.CSS, using = "td.multiplePax > table.0.3")
    @FindBy(how = How.NAME, using = "modifiedDetail.roomsList[0].roomNames[0].lastName")
    public  WebElement paxsSurname1;


    //    @FindBy(how = How.CSS, using = "td.multiplePax > table.1.1")
    @FindBy(how = How.NAME, using = "modifiedDetail.roomsList[0].roomNames[1].title")
    public  WebElement paxtitle2;


    //    @FindBy(how = How.CSS, using = "td.multiplePax > table.1.2")
    @FindBy(how = How.NAME, using = "modifiedDetail.roomsList[0].roomNames[1].firstName")
    public  WebElement paxfirstName2;

    //    @FindBy(how = How.CSS, using = "td.multiplePax > table.1.3")
    @FindBy(how = How.NAME, using = "modifiedDetail.roomsList[0].roomNames[1].lastName")
    public  WebElement paxSurnameName2;


    @FindBy(how = How.LINK_TEXT, using = "Continue")
    public  WebElement continuee;
}
