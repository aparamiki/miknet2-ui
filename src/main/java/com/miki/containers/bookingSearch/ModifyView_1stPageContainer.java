package com.miki.containers.bookingSearch;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 11/02/15.
 */
public class ModifyView_1stPageContainer {


    @FindBy(how = How.CSS, using = "h1")
    public WebElement headerModifyBooking;



    @FindBy(how = How.CSS, using = "a.title")
    public WebElement hotelName;


    @FindBy(how = How.ID, using = "numberOfNights")
    public static WebElement numberOfNights;



    /**
     * DoubleRoom
     */

    // double Room Number
    @FindBy(how = How.CSS, using = "table.1.1")
    public WebElement doubleRoomsNumber;

    //double room with Extra Beds
    @FindBy(how = How.CSS, using = "table.1.2")
    public WebElement doubleRoomsWithExtraBeds;

    //double room with cot
    @FindBy(how = How.CSS, using = "table.1.3")
    public WebElement doubleRoomsWithCot;


    /**
     * twin room
     */
    // twin Room Number
    @FindBy(how = How.CSS, using = "table.2.1")
    public WebElement twinRoomsNumber;

    //twin room with Extra Beds
    @FindBy(how = How.CSS, using = "table.2.2")
    public WebElement twinRoomsWithExtraBeds;

    //twin room with cot
    @FindBy(how = How.CSS, using = "table.2.3")
    public WebElement twinRoomsWithCot;


    /**
     * Single Room
     */

    @FindBy(how = How.CSS, using = "table.3.1")
    public WebElement singleRoomsNumber;

    //twin room with Extra Beds
    @FindBy(how = How.CSS, using = "table.3.3")
    public WebElement singleRoomsWitCot;


    //Continue
    @FindBy(how = How.LINK_TEXT, using = "Continue")
    public WebElement continueButton;



}
