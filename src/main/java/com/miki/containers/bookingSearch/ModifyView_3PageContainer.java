package com.miki.containers.bookingSearch;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 17/02/15.
 */
public class ModifyView_3PageContainer {

    @FindBy(how = How.LINK_TEXT, using = "Complete")
    public WebElement completeModification;

}
