package com.miki.containers.bookingSearch;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 28/07/14.
 */
public class SearchBookingContainer {


    public static String PageTitle = "mikiNet - Manage bookings";

    @FindBy(how = How.ID, using = "bookingRef")
    public WebElement bookingRef;


    @FindBy(how = How.LINK_TEXT, using = "Search")
    public WebElement search;


}
