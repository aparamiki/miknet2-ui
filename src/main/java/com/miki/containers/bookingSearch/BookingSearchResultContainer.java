package com.miki.containers.bookingSearch;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 28/07/14.
 */
public class BookingSearchResultContainer {


    public static String PageTitle = "mikiNet - Booking search results";


    @FindBy(how = How.CSS, using = "table[class='bookingResults bookingSearchResults'] > tbody > tr > td")
//    @CacheLookup
    public WebElement clientName;

    @FindBy(how = How.CSS, using = "table[class='bookingResults bookingSearchResults'] > tbody > tr > td:nth-child(2)")
//    @CacheLookup
    public WebElement bookingDate;


    @FindBy(how = How.CSS, using = "table[class='bookingResults bookingSearchResults'] > tbody > tr > td[class='tourRef']")
//    @CacheLookup
    public WebElement mikiReference;



    @FindBy(how = How.CSS, using = "table[class='bookingResults bookingSearchResults'] > tbody > tr > td[class='product']")
//    @CacheLookup
    public WebElement productName;


    @FindBy(how = How.CSS, using = "table[class='bookingResults bookingSearchResults'] > tbody > tr > td:nth-child(5)")
//    @CacheLookup
    public WebElement serviceDate;


   @FindBy(how = How.CSS, using = "table[class='bookingResults bookingSearchResults'] > tbody > tr > td:nth-child(6)")
//   @CacheLookup
   public WebElement bookingStatus;

  @FindBy(how = How.CSS, using = "table[class='bookingResults bookingSearchResults'] > tbody > tr > td:nth-child(7)")
//  @CacheLookup
    public WebElement subTotalPrice;


    @FindBy(how = How.CSS, using = "span.totalPrice")
//    @CacheLookup
    public WebElement totalPrice;


    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    public WebElement cancelBookingLink;



    @FindBy(how = How.LINK_TEXT, using = "Modify")
    public WebElement modifyBookingLink;


    @FindBy(how = How.LINK_TEXT, using = "Logout")
//    @CacheLookup
    public WebElement logout;










}
