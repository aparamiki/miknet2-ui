package com.miki.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 02/07/14.
 */
public class BookHotel_CheckAvailabilityContainer {

    public static String HOTELSEARCHRESULTPAGETITLE = "mikiNet - Rooming list";


    @FindBy(how = How.ID, using = "LB_01_00001_0")
    public WebElement roomListSelectBox;

    @FindBy(how = How.ID, using = "checkbutton")
    public WebElement checkAvailability;


    @FindBy(how = How.CLASS_NAME,  using = "roomAvailability")
    public WebElement checkAvailabilityTable;


    @FindBy(how = How.CSS,  using = "tr#01")
    public WebElement tablerow;




}
