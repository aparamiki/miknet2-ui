package com.miki.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 03/07/14.
 */
public class BookingRefCompleteContainter {

    @FindBy(how = How.ID, using = "yourRef")
    public WebElement yourReference;

    @FindBy(how = How.NAME, using = "termsAndConditions")
    public WebElement termsAndConditionsCheckBox;


    @FindBy(how = How.LINK_TEXT, using = "Complete")
    public WebElement completeBooking;
}
