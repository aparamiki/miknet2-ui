package com.miki.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 03/07/14.
 */
public class BookingCompletedSuccessfullyContainer {


    @FindBy(how = How.CSS, using = "h1")
    public WebElement bookingCompleteTitle;

    @FindBy(how = How.CSS, using = "div.bookingReferences > p")
    public WebElement bookingReferenceNumber;


    //booking type : hotel
    @FindBy(how = How.CSS, using = "div.bookingType > p")
    public WebElement bookingProduct;


    // booking date
    @FindBy(how = How.CSS, using = "div.date > p")
    public WebElement bookingDate;

    // miki reference number
    @FindBy(how = How.CSS, using = "p.mikiRef")
    public WebElement mikibookingReferenceNumber;


//    Booking type : confirmed
    @FindBy(how = How.CSS, using = "strong")
    public WebElement mikiConfirmedStatus;


    @FindBy(how = How.LINK_TEXT, using = "Logout")
    public WebElement logout;
}
