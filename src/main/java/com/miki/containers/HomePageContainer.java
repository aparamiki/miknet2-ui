package com.miki.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 24/06/14.
 */
public class HomePageContainer {


    public static String HOMEPAGETITLE = "mikiNet - Home";

    @FindBy(how = How.CSS, using = "p.loggedUserInfo > span")
    public WebElement loggeduser;


    @FindBy(how = How.CSS, using = "h1")
    public WebElement home;

    @FindBy(how = How.LINK_TEXT, using = "Logout")
    public WebElement logout;

    @FindBy(how = How.LINK_TEXT, using = "Book")
    public WebElement book;


    @FindBy(how = How.LINK_TEXT, using = "Bookings")
    public WebElement bookings;

}
