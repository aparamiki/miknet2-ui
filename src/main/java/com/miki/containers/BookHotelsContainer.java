package com.miki.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 02/07/14.
 */
public class BookHotelsContainer {


    @FindBy(how = How.NAME, using = "bookingDetail.roomsList[0].roomNames[0].title")
    public WebElement paxTitle1;


    @FindBy(how = How.NAME, using = "bookingDetail.roomsList[0].roomNames[0].firstName")
    public WebElement paxFirstName1;


    @FindBy(how = How.NAME, using = "bookingDetail.roomsList[0].roomNames[0].lastName")
    public WebElement paxLastName1;


    @FindBy(how = How.NAME, using = "bookingDetail.roomsList[0].roomNames[1].title")
    public WebElement paxTitle2;


    @FindBy(how = How.NAME, using = "bookingDetail.roomsList[0].roomNames[1].firstName")
    public WebElement paxFirstName2;



    @FindBy(how = How.NAME, using = "bookingDetail.roomsList[0].roomNames[1].lastName")
    public WebElement paxLastName2;



    @FindBy(how = How.NAME, using = "bookingDetail.roomsList[0].roomNames[2].title")
    public WebElement paxTitle3;


    @FindBy(how = How.NAME, using = "bookingDetail.roomsList[0].roomNames[2].firstName")
    public WebElement paxFirstName3;



    @FindBy(how = How.NAME, using = "bookingDetail.roomsList[0].roomNames[2].lastName")
    public WebElement paxLastName3;


    @FindBy(how = How.CSS, using = "div.form > div.buttonGroup > span.forwardButtonOuter > span.forwardButtonInner > input.primary")
    public WebElement addToBasketButton;


    @FindBy(how = How.CLASS_NAME, using = "roomAvailability")
    public WebElement checkAvailabilityTable;


}
