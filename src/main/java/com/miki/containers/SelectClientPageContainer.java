package com.miki.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 26/06/14.
 */
public class SelectClientPageContainer {

    public  String CLIENTPAGETITLE = "mikiNet - Select client";

    @FindBy(how = How.ID, using = "ajaxFieldValue2")
    public WebElement clientCode;


    @FindBy(how = How.ID, using = "users")
    public WebElement username;

    @FindBy(how = How.ID, using = "selectDiv_ajaxFieldValue2_0")
    public WebElement clientcodevalue;


    @FindBy(how = How.CSS, using = "input.primary")
    public WebElement submit;

    @FindBy(how = How.XPATH, using = "//input[@value='Reset']")
    public WebElement reset;



    // cannnot initialse since ajax list
//    @FindBy(how = How.ID, using = "Apara Sharma (apara)")
//    public WebElement clientName;
}
