package com.miki.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by asharma on 30/06/14.
 */
public class HotelSearchContainer {

    public static String HOTELSEARCHPAGETITLE = "mikiNet - Hotel search";


    @FindBy(how = How.ID, using = "paxDomicile")
    public WebElement paxDomicile;


    @FindBy(how = How.ID, using = "ajaxCityName")
    public WebElement cityName;


    @FindBy(how = How.ID, using = "selectDiv_ajaxCityName_0")
    public WebElement cityNameValue;


    @FindBy(how = How.ID, using = "countryCode")
    public WebElement countryCode;

    @FindBy(how = How.ID, using = "cityCode")
    public WebElement cityCode;

    @FindBy(how = How.ID, using = "hotelName")
    public WebElement hotelName;


    @FindBy(how = How.ID, using = "checkInDate")
    public WebElement checkInDate;

    @FindBy(how = How.ID, using = "numberOfNightsDisplay")
    public WebElement nights;


    @FindBy(how = How.CSS, using = "input.primary")
    public WebElement SearchButton;


    @FindBy(how = How.ID, using = "currency")
    public WebElement currency;
}
