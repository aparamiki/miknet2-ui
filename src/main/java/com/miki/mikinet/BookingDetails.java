package com.miki.mikinet;

/**
 * Created by asharma on 16/07/14.
 */
public class BookingDetails {


    private String bookingproduct;
    private String serviceDate;
    private String bookingref;
    private String bookingmikiref;
    private String bookingstatus;


    public BookingDetails(String bproduct, String sdate, String bref, String bmikiref, String bstatus) {
        this.bookingproduct = bproduct;
        this.serviceDate = sdate;
        this.bookingref = bref;
        this.bookingmikiref = bmikiref;
        this.bookingstatus = bstatus;
        System.out.println(this.toString());
    }


    public BookingDetails() {
        System.out.println("inside booking detail class");
    }

    public BookingDetails withReferenceNumber(String referenceNumber) {
        this.bookingref = referenceNumber;
        return this;
    }


    public String getBookingproduct() {
        return bookingproduct;
    }

    public String getserviceDate() {
        return serviceDate;
    }


    public String getBookingRef() {
        return bookingref;
    }

    @Override
    public String toString() {
        return bookingproduct + "," + serviceDate + "," + bookingref + " " + bookingmikiref + " " + bookingstatus;
    }


    public String getbookingmikiref() {
        return bookingmikiref;
    }

    public String getbookingstatus() {
        return bookingstatus;
    }

}

