package com.miki.mikinet;

import com.miki.utils.BrowserDriver;
import com.miki.views.*;
import com.miki.views.bookingSearch.*;
import org.junit.Assert;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Created by asharma on 12/06/14.
 */
public class Navigation {
    private User user;
    BookingDetails bd;
    BookingDetails bookingDetails;
    private static final Logger LOGGER = Logger.getLogger(Navigation.class.getName());


    /**
     * adding new variable to call booking search function again if no search results are  returned back in the original request
     */
    String paxDomicile, city, geolocation, hotel, date, currency;


    public boolean given_I_navigate_to_mikinet_application() {
        BrowserDriver.loadPage("http://test.mikinet.co.uk");

        if (!(LoginPageView.isDisplayedCheck())) {
            LOGGER.info("Not Login Page");
            Assert.fail();
        }
        return LoginPageView.isDisplayedCheck();
    }


    /**
     * 10/02/2015 apara
     * ModifyBooking
     *
     * @return boolean
     */


    public boolean given_I_navigate_to_mikinet_application(String url) {
        BrowserDriver.loadPage(url);
        return LoginPageView.isDisplayedCheck();
    }


    //used by feature - BookaHotel
    public void when_I_try_to_login(String clientcode, String userid, String password) {
        LOGGER.info("Miki Login Page");
        LoginPageView.loginToPage(clientcode, userid, password);
    }

    // used by Feature - Login.feature
    public void when_I_try_to_login(List<User> actualUsers) {
        LOGGER.info("Entering User details");
        for (User temp : actualUsers) {
            LoginPageView.loginToPage(temp.getClientCode(), temp.getUsername(), temp.getPassword());
        }
    }

    public boolean then_I_should_see_the_homepage() {
        return HomePage_UserView.isDisplayedCheck();
    }


    public String getLoggedInUserName() {
        return HomePage_UserView.LoggedInUser();
    }

    public void Then_I_should_logout_by_clicking_on_logout_link() {
        HomePage_UserView.logOut();
    }

    public void I_click_on_Book_and_select_Hotels() {
        HomePage_UserView.ClickBookandSelectHotelDropDown();
    }


    public void Then_I_Should_Client_Page(String clientcode, String clientname, String Username) {
        SelectClientView.newSetClientCodeName(clientcode, clientname);
        SelectClientView.setUserNameandSubmit(Username);
    }


    public boolean When_I_Search_For_Hotel(String paxDomicileV, String cityV, String geolocationV, String hotelV, String dateV, String currencyV) throws Exception {
        int increment = 60;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
        assertTrue(HotelSearchView.isDisplayedCheck());
        paxDomicile = paxDomicileV;
        city = cityV;
        geolocation = geolocationV;
        hotel = hotelV;
        currency = currencyV;
        int i;
        for (i = 0; i < 5; i++) {
            cal.add(Calendar.DATE, increment);
            date = format1.format(cal.getTime());

            HotelSearchView.enterHotelSearchDetails(paxDomicile, city, geolocation, hotel, date, currency);
            if (!(HotelSearchResultView.confirmSearchHotelResult().contentEquals("0 hotel found"))) {
                HotelSearchResultView.bookHotel();
                break;
            } else {
                HotelSearchResultView.clickbackButton();
                increment = +15;
            }
        }
        if (i == 5) return false;
        else return true;

    }


    public void When_I_Select_Hotel_From_List() {
//        HotelSearchResultView.bookHotel();

    }


    public void check_availability_of_hotel() {
        BookHotel_CheckAvailabilityView.checkRoomAvaiability();
        BookHotelView.setPaxDetailForPassanger();
    }


    public void bookingbasket_complete_booking() {
        BookingBasketView.completeBooking();
    }

    public void enterBookingReference() {
        BookingRefCompleteView.enterRefandCompleteBooking();
    }

    public void completeBooking() {
//        BookingCompletedSuccessfullyView.captureValues();
        assertNotNull(BookingCompletedSuccessfullyView.getBookingReference());
        LOGGER.info("Booking reference : " + BookingCompletedSuccessfullyView.getBookingReference());
        assertNotNull(BookingCompletedSuccessfullyView.getMikiBookingReference());
//        bookingDetails = new BookingDetails(BookingCompletedSuccessfullyView.getBookingProduct(), BookingCompletedSuccessfullyView.getBookingDate(), BookingCompletedSuccessfullyView.getBookingReference(), BookingCompletedSuccessfullyView.getMikiBookingReference(), BookingCompletedSuccessfullyView.getmikiConfirmedStatus());
//        ListbookingsCreated.add(bookingDetails);
//

    }

    // Operation : Cancel, Modify
    public void I_click_on_Booking_and_select_SearchBooking(int index) {
        HomePage_UserView.selectSearchBookingOptionsa();
        assertTrue(ManageBooking.isDisplayedCheck());
        searchForBookingWithID(index);
    }


    public void captureBookingDetails() {
        bookingDetails = new BookingDetails(BookingCompletedSuccessfullyView.getBookingProduct(), BookingCompletedSuccessfullyView.getBookingDate(), BookingCompletedSuccessfullyView.getBookingReference(), BookingCompletedSuccessfullyView.getMikiBookingReference(), BookingCompletedSuccessfullyView.getmikiConfirmedStatus());
        BrowserDriver.getBookingArrayList().add(bookingDetails);

//        Iterator<BookingDetails> iterator = BrowserDriver.getBookingArrayList().iterator();
//        while (iterator.hasNext()) {
//            System.out.println(iterator.next().toString());
//        }
        readCreatedBooking();

    }


    public void Then_I_should_logout_by_clicking_on_logout_link_at_bookingcompletepage() {
        BookingCompletedSuccessfullyView.logout();

    }


    //apara 23 july
//    public void runTidyUpScript() {
//
//        for (BookingDetails temp : BrowserDriver.getBookingArrayList()) {
//            System.out.println(temp.toString());
//        }
//    }


//    @After
//    public void tearDown(Scenario scenario) {
//        if (scenario.isFailed()) {
//            // Take a screenshot...
//            final byte[] screenshot = ((TakesScreenshot) BrowserDriver.getCurrentDriver()).getScreenshotAs(OutputType.BYTES);
//            scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
//        }
//    }


//    publpublic void When_I_Select_Hotel_From_List() {
//    }ic void when_I_try_to_login(String credentialsType) {
//        CredentialsType ct = CredentialsType.credentialsTypeForName(credentialsType);
//        switch (ct) {
//            case VALID:
//          public void given_I_navigate_to_login() {
//    }      user = Users.createValidUser();
//             break;
//            case INVALID:
//                user = Users.createInvalidUser();
//                break;
//        }
//        LoginView.login(user.getUsername(), user.getPassword());
//    }
//
//    public void then_I_login(String outcomeString) {
//        Outcome outcome = Outcome.outcomeForName(outcomeString);
//        switch (outcome) {
//            case SUCCESS:
//                LoginView.checkLoginSuccess();
//                break;
//            case FAILURE:
//                LoginView.checkLoginErrors();
//                break;
//        }
//    }


    //Apara 28 july
    public void readCreatedBooking() {
        Iterator<BookingDetails> iterator = BrowserDriver.getBookingArrayList().iterator();
        while (iterator.hasNext()) {
            System.out.println("Booking Details " + iterator.next().toString());
        }
    }


    public void searchForBookingWithID(int index) {
//        BookingDetails bd = (BookingDetails) BrowserDriver.getBookingArrayList().get(index);
        bd = (BookingDetails) BrowserDriver.getBookingArrayList().get(index);
        ManageBooking.enterBookingReferenceNumber(bd.getBookingRef().substring(18));
        //Check OnRequest logic working fine
//        ManageBooking.enterBookingReferenceNumber("2474647");

    }


    public void I_should_see_the_hotel_booking_details() {
        BookingSearchResult.getBookingValues();
        assertEquals("true", BookingSearchResult.bookingStatus(), bd.getbookingstatus());
        assertEquals("true", BookingSearchResult.mikiReference(), bd.getbookingmikiref().substring(16));
//     BookingSearchResult.clickLogout();
    }


    public boolean I_should_be_able_cancel_booking() {
        BookingSearchResult.cancelBooking();

        if (CancelBookingView.isDisplayedCheck()) {
            CancelBookingView.cancelBookingItem();
            return true;
        } else
            return false;
   }


    public boolean I_should_also_see_cancallation_Confirmed_page() {
        // cancel booking confirmationpage  displayed or not
        if (CancelBookingConfirmed_View.isDisplayedCheck()) {

            LOGGER.info("Booking Cancellation Confirmation page displayed. Now logging Out");
            CancelBookingConfirmed_View.logout();
            return true;
        } else {
            LOGGER.info("Booking Cancellation Confirmation page NOT displayed. Some Thing Wrong");
            return false;
        }
    }

    public void I_modify_the_booking_by_changing_number_of_Nights_and_Name(String nights, String paxtitle1, String paxname1, String paxsurname1, String paxtitle2, String paxname2, String paxsurname2) {
        BookingSearchResult.modifyBooking();
        ModifyBooking_1stPage_View.TwinRoom_changeNights(nights);
        ModifyBooking_1stPage_View.ContinueToNextPage();
        ModifyBooking_2ndPage_View.changePaxNameforTwinRoom(paxtitle1, paxname1, paxsurname1, paxtitle2, paxname2, paxsurname2);
    }


    public boolean check_booking_status_to_modify(String arg1) {
        if (!(BookingSearchResult.bookingStatus().equals(arg1)))
            return false;
        else return true;

    }

    public void completeModification() {
        ModifyView_3Page_View.completeModification();
    }

    public boolean confirmModificationCompletion() {
     return  ModificationComplete_4thPage_View.modificationComplete();
    }
}
