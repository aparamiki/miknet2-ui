package com.miki.mikinet;

/**
 * Created by asharma on 12/06/14.
 */
public class NavigationFactory {

    private static Navigation navigator = null;


    public static synchronized Navigation getNavigator() {
        if (navigator == null) {
            navigator = new Navigation();
        }
        return navigator;
    }
}
