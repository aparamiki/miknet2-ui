package com.miki.mikinet;

/**
 * Created by asharma on 12/06/14.
 */
public class User {
    private String clientcode;
    private String username;
    private String password;


    public User(String code, String name, String pword) {
        this.clientcode = code;
        this.username = name;
        this.password = pword;
    }


    public User withUserName(String username) {
        this.username = username;
        return this;
    }


    public String getUsername() {
        return username;
    }

    public String getClientCode() {
        return clientcode;
    }


    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return clientcode + "," + username + "," + password;
    }


}
