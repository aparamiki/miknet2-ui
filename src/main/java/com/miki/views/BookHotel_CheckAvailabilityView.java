package com.miki.views;

import com.miki.containers.BookHotel_CheckAvailabilityContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by asharma on 02/07/14.
 */
public class BookHotel_CheckAvailabilityView {

    private static final BookHotel_CheckAvailabilityContainer hotelAvailibilityContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), BookHotel_CheckAvailabilityContainer.class);


    public static void checkRoomAvaiability() {
        BrowserDriver.selectDropDownValue(BrowserDriver.readSelectElementFromTable(hotelAvailibilityContainer.checkAvailabilityTable),
                "1");

        hotelAvailibilityContainer.checkAvailability.click();
    }


}
