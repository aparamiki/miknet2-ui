package com.miki.views;

import com.miki.containers.BookingRefCompleteContainter;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 03/07/14.
 */
public class BookingRefCompleteView {


    private static final BookingRefCompleteContainter bookingRefContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), BookingRefCompleteContainter.class);
    private static final Logger LOGGER = Logger.getLogger(BookingRefCompleteView.class.getName());

    public static void enterRefandCompleteBooking()
    {
        bookingRefContainer.yourReference.sendKeys("TESTREF");
        bookingRefContainer.termsAndConditionsCheckBox.click();
        bookingRefContainer.completeBooking.click();

    }
}
