package com.miki.views;

import com.miki.containers.BookingBasketContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 03/07/14.
 */
public class BookingBasketView {

    private static final BookingBasketContainer bookingBasketContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), BookingBasketContainer.class);
    private static final Logger LOGGER = Logger.getLogger(BookingBasketView.class.getName());

    public static void completeBooking()
    {
        bookingBasketContainer.complete.click();
    }

}
