package com.miki.views;

import com.miki.containers.HotelSearchResultContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 02/07/14.
 */
public class HotelSearchResultView {

    private static final HotelSearchResultContainer hotelSearchResultContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), HotelSearchResultContainer.class);
    private static final Logger LOGGER = Logger.getLogger(HotelSearchResultView.class.getName());

    public static void bookHotel() {
        LOGGER.info("Hotel Search Result - Selecting the first hotel");
        hotelSearchResultContainer.bookHotel1.click();
    }


    public static String confirmSearchHotelResult() {

        LOGGER.info("City Search Result   " + hotelSearchResultContainer.NumberOfHotelsReturnedText.getText());
        return hotelSearchResultContainer.NumberOfHotelsReturnedText.getText();

    }
    public static void clickbackButton()
    {
        if ((hotelSearchResultContainer.backButton.isDisplayed()) == true)
        {
        hotelSearchResultContainer.backButton.click();
    }
    else LOGGER.info("Element not visible");
    }

}
