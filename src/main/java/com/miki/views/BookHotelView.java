package com.miki.views;

import com.miki.containers.BookHotelsContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by asharma on 03/07/14.
 */
public class BookHotelView {

    private static final BookHotelsContainer bookHotelContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), BookHotelsContainer.class);

    private static String roomType = null;

    public static void setPaxDetailForPassanger() {

        BrowserDriver.selectDropDownValue(bookHotelContainer.paxTitle1, "Not specified");
        bookHotelContainer.paxFirstName1.sendKeys("Apara");
        bookHotelContainer.paxLastName1.sendKeys("Sharma");

        roomType = BrowserDriver.readElementFromTable(bookHotelContainer.checkAvailabilityTable).getText();

        if (!(roomType.equals("Single room"))) {
            BrowserDriver.selectDropDownValue(bookHotelContainer.paxTitle2, "Mr");
            bookHotelContainer.paxFirstName2.sendKeys("A");
            bookHotelContainer.paxLastName2.sendKeys("B");
            bookHotelContainer.addToBasketButton.click();

            if (roomType.equals("Triple room")) {
                BrowserDriver.selectDropDownValue(bookHotelContainer.paxTitle2, "Mrs");
                bookHotelContainer.paxFirstName2.sendKeys("C");
                bookHotelContainer.paxLastName2.sendKeys("D");
                bookHotelContainer.addToBasketButton.click();

            }
        }


    }


}
