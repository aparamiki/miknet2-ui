package com.miki.views;

import com.miki.containers.LoginPageContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 12/06/14.
 */
public class LoginPageView {


    private static final LoginPageContainer loginContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), LoginPageContainer.class);


    private static final Logger LOGGER = Logger.getLogger(LoginPageView.class.getName());

    public static boolean isDisplayedCheck() {
        if (loginContainer.logInForm.isDisplayed()) {
            LOGGER.info("Login Form Displayed");
            return true;
        } else {
            LOGGER.info("Login Form Not Displayed");
            return false;
        }
    }


    public static void loginToPage(String clientCode, String userid, String password) {
        LOGGER.info("Trying to enter login details");
        if ((loginContainer.clientCode.isDisplayed()) &&
                (loginContainer.usernameInput.isDisplayed()) && (loginContainer.passwordInput.isDisplayed())) {
            LOGGER.info("Login Page Loaded.");
            loginContainer.clientCode.sendKeys(clientCode);
            loginContainer.usernameInput.sendKeys(userid);
            loginContainer.passwordInput.sendKeys(password);
            loginContainer.submitButton.click();
        } else {
            LOGGER.info("ReLoading the page");
            BrowserDriver.reopenAndLoadPage("https://test.mikinet.co.uk");
            try {
                Thread.sleep(3000);
            } catch (Exception e) {

            }
            loginToPage(clientCode, userid, password);
        }

    }
}
