package com.miki.views;

import com.miki.containers.BookingCompletedSuccessfullyContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 03/07/14.
 */
public class BookingCompletedSuccessfullyView {

    private static final Logger LOGGER = Logger.getLogger(BookingCompletedSuccessfullyView.class.getName());
    private static final BookingCompletedSuccessfullyContainer bookingCompleteContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), BookingCompletedSuccessfullyContainer.class);

    public static String getBookingReference() {
        LOGGER.info("Booking reference number is : " +  bookingCompleteContainer.bookingReferenceNumber.getText());
        return bookingCompleteContainer.bookingReferenceNumber.getText();
    }


    public static String getMikiBookingReference() {

        LOGGER.info("Miki Booking reference number is : " +  bookingCompleteContainer.bookingReferenceNumber.getText());
        return bookingCompleteContainer.mikibookingReferenceNumber.getText();
    }

    public static String getBookingProduct() {
       return  bookingCompleteContainer.bookingProduct.getText();
    }


    public static String getBookingDate() {
        return  bookingCompleteContainer.bookingDate.getText();
    }


    public static String getmikiConfirmedStatus() {
        return  bookingCompleteContainer.mikiConfirmedStatus.getText();
    }

    public static void logout() {

        bookingCompleteContainer.logout.click();
    }
}
