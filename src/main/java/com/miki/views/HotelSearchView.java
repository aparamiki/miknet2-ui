package com.miki.views;

import com.miki.containers.HotelSearchContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 30/06/14.
 */
public class HotelSearchView {

    private static final HotelSearchContainer hotelSearchContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), HotelSearchContainer.class);
    private static final Logger LOGGER = Logger.getLogger(HotelSearchView.class.getName());

    public static boolean isDisplayedCheck() {
        if (BrowserDriver.getPageTitle().trim().equalsIgnoreCase(hotelSearchContainer.HOTELSEARCHPAGETITLE)) {
            LOGGER.info("Hotel Search Page");
            return true;
        } else {
            System.out.println("Search page Title mismatch.Not A search page");
            return false;
        }
    }


    public static void enterHotelSearchDetails(String paxDomicile, String city, String geolocation, String hotel, String date, String currency) {
        if (isDisplayedCheck()) {
            LOGGER.info("HotelSearch Page : Enter Search Criteria");
            setPaxDomicile(paxDomicile);
            setHotelSearchCity(city, geolocation);
            hotelSearchContainer.hotelName.sendKeys(hotel);
            hotelSearchContainer.checkInDate.clear();
//            System.out.println("checkInDate  " + date);
            hotelSearchContainer.checkInDate.sendKeys(date);
            hotelSearchContainer.checkInDate.sendKeys(Keys.RETURN);
//            BrowserDriver.selectDropDownValue(hotelSearchContainer.nights, nights);
            hotelSearchContainer.SearchButton.click();
        }
    }

    public static void setPaxDomicile(String s) {
        BrowserDriver.selectDropDownValue(hotelSearchContainer.paxDomicile, s);
    }


    public static void setHotelSearchCity(String city, String geolocation) {
        int max = 10;
        String text = null;
        start:
        for (int i = 0; i < max; i++) {
            hotelSearchContainer.cityName.clear();
            hotelSearchContainer.cityName.sendKeys(city);
            try {
                Thread.sleep(2000);
                text = hotelSearchContainer.cityNameValue.getText();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (text.trim().compareTo(geolocation.trim()) == 0) {
                hotelSearchContainer.cityNameValue.click();
                break;
            } else
                hotelSearchContainer.cityName.clear();
            continue start;
        }
        // this delay is put to sync the time for city to be entered , before entering submit
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}





