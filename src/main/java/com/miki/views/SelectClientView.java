package com.miki.views;

import com.miki.containers.SelectClientPageContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 26/06/14.
 */
public class SelectClientView {

    private static final SelectClientPageContainer selectClientContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), SelectClientPageContainer.class);
    private static final Logger LOGGER = Logger.getLogger(SelectClientView.class.getName());

    public static boolean isDisplayedCheck() {
        if (BrowserDriver.getPageTitle().equals(selectClientContainer.CLIENTPAGETITLE)) {
            LOGGER.info("Select Client Page");
            return true;
        } else

            return false;
    }



    public static void newSetClientCodeName(String clientCode, String clientname) {
        int max = 10;
        String text = null;
        start:
        for (int i = 0; i < max; i++) {
            selectClientContainer.clientCode.sendKeys(clientCode);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (selectClientContainer.clientcodevalue.isDisplayed()) {
                text = selectClientContainer.clientcodevalue.getText();
                if (text.compareTo(clientname) == 0) {
                    selectClientContainer.clientcodevalue.click();
                    break;

                } else {
                    System.out.println("Lets Try Again");
                    selectClientContainer.reset.click();
                }
            } else {
                System.out.println("Element Not Displayed");
                selectClientContainer.reset.click();
            }
        }
    }


    public static void setUserNameandSubmit(String s) {
        BrowserDriver.selectDropDownValue(selectClientContainer.username, s);
        BrowserDriver.waitForElement(selectClientContainer.submit).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
