package com.miki.views;

import com.miki.containers.HomePageContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 24/06/14.
 */
public class HomePage_UserView {

    private static final HomePageContainer homeContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), HomePageContainer.class);
    private static final Logger LOGGER = Logger.getLogger(HomePage_UserView.class.getName());

    public static boolean isDisplayedCheck() {
        if (BrowserDriver.getPageTitle().equals(homeContainer.HOMEPAGETITLE)) {
            return true;
        } else
            return false;
    }


    public static void logOut() {
        BrowserDriver.waitForElement(homeContainer.logout).click();
    }

    public static String LoggedInUser() {
        return BrowserDriver.waitForElement(homeContainer.loggeduser).getText().trim();
    }

    public static void ClickBookandSelectHotelDropDown() {
        BrowserDriver.performMouseOver_and_moveToNewElement(homeContainer.book, "a[title='Hotels']", "CSS");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void selectSearchBookingOptionsa() {
        BrowserDriver.performMouseOver_and_moveToNewElement(homeContainer.bookings, "a[title='Booking search']", "CSS");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
