package com.miki.views.bookingSearch;

import com.miki.containers.bookingSearch.ModificationComplete_4thPage_Containter;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 20/02/15.
 */
public class ModificationComplete_4thPage_View {


    private static final ModificationComplete_4thPage_Containter modifyContainer4 = PageFactory.initElements(BrowserDriver.getCurrentDriver(), ModificationComplete_4thPage_Containter.class);

    private static final Logger LOGGER = Logger.getLogger(ModificationComplete_4thPage_View.class.getName());


    public static boolean modificationComplete() {

        LOGGER.info("modifyContainer4.modificationCompleteHeader.getText  : " + modifyContainer4.modificationCompleteHeader.getText());
        if (modifyContainer4.modificationCompleteHeader.getText().equals("Modification completed")) {
            LOGGER.info("Modification Successfully Completed");
            modifyContainer4.Logout.click();

            return true;
        } else return false;
    }


}
