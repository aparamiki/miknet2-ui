package com.miki.views.bookingSearch;

import com.miki.containers.bookingSearch.ModifyView_1stPageContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 11/02/15.
 */
public class ModifyBooking_1stPage_View {


    private static final ModifyView_1stPageContainer mod_1stpageContianter = PageFactory.initElements(BrowserDriver.getCurrentDriver(), ModifyView_1stPageContainer.class);


    private static final Logger LOGGER = Logger.getLogger(ModifyBooking_1stPage_View.class.getName());


    public void changeNights(int num) {

    }


    public void clickContinueButton() {

    }


    public static void TwinRoom_changeNights(String nights) {

        BrowserDriver.selectDropDownValue(mod_1stpageContianter.numberOfNights,  nights);
     

    }


    public static void ContinueToNextPage()
    {
        mod_1stpageContianter.continueButton.click();
    }
}
