package com.miki.views.bookingSearch;


import com.miki.containers.bookingSearch.ModifyView_3PageContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 19/02/15.
 */
public class ModifyView_3Page_View {

    private static final ModifyView_3PageContainer modifyContainer3 = PageFactory.initElements(BrowserDriver.getCurrentDriver(), ModifyView_3PageContainer.class);

    private static final Logger LOGGER = Logger.getLogger(ModifyView_3Page_View.class.getName());


    public static void completeModification()
    {
       LOGGER.info("Completing the modification");
        modifyContainer3.completeModification.click();
    }

}
