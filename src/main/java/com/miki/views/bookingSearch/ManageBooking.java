package com.miki.views.bookingSearch;

import com.miki.containers.bookingSearch.SearchBookingContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 17/07/14.
 */
public class ManageBooking {


    private static final SearchBookingContainer searchBooking = PageFactory.initElements(BrowserDriver.getCurrentDriver(), SearchBookingContainer.class);
    private static final Logger LOGGER = Logger.getLogger(ManageBooking.class.getName());

    public static boolean isDisplayedCheck() {
        LOGGER.info("Searching for Booking");
        LOGGER.info(searchBooking.PageTitle);
        // check if page loaded successfully. Title is displayed
        if (BrowserDriver.getPageTitle().trim().equalsIgnoreCase(searchBooking.PageTitle)) {

            return true;
        } else {
            LOGGER.info("Not in a SearchBooking Page. Error");
            return false;
        }
    }


    public static void enterBookingReferenceNumber(String bookingRef) {
        LOGGER.info("Entering the Booking Reference");
        searchBooking.bookingRef.sendKeys(bookingRef);
        searchBooking.search.click();

    }


}
