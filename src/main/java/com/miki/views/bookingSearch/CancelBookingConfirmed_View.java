package com.miki.views.bookingSearch;

import com.miki.containers.bookingSearch.CancelBookingConfirmed_Containter;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 20/02/15.
 */
public class CancelBookingConfirmed_View {


    private static final CancelBookingConfirmed_Containter cancelbooking2 = PageFactory.initElements(BrowserDriver.getCurrentDriver(), CancelBookingConfirmed_Containter.class);
    private static final Logger LOGGER = Logger.getLogger(CancelBookingConfirmed_View.class.getName());

    public static boolean isDisplayedCheck() {
        LOGGER.info(cancelbooking2.cancellationConfirmedHeader.getText());
        if (cancelbooking2.cancellationConfirmedHeader.getText() != null) {

            return true;
        } else {
            LOGGER.info("Booking Not onCancellationPage");
            return false;
        }
    }


    public static void logout() {

        cancelbooking2.logout.click();
    }
}
