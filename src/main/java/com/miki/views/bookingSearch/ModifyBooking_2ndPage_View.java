package com.miki.views.bookingSearch;

import com.miki.containers.bookingSearch.ModifyView_2ndPageContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 13/02/15.
 */
public class ModifyBooking_2ndPage_View {

    private static final ModifyView_2ndPageContainer modifyContainer2 = PageFactory.initElements(BrowserDriver.getCurrentDriver(), ModifyView_2ndPageContainer.class);

    private static final Logger LOGGER = Logger.getLogger(ModifyBooking_2ndPage_View.class.getName());

    public static void changePaxNameforTwinRoom(String paxtitle1, String paxname1, String paxsurname1, String paxtitle2, String paxname2, String paxsurname2) {


        BrowserDriver.selectDropDownValue(modifyContainer2.paxtitle1, paxtitle1);


        modifyContainer2.paxfirstname1.clear();
        modifyContainer2.paxfirstname1.sendKeys(paxname1);


        modifyContainer2.paxsSurname1.clear();
        modifyContainer2.paxsSurname1.sendKeys(paxsurname1);

        BrowserDriver.selectDropDownValue(modifyContainer2.paxtitle2, paxtitle2);

        modifyContainer2.paxfirstName2.clear();
        modifyContainer2.paxfirstName2.sendKeys(paxname2);

        modifyContainer2.paxSurnameName2.clear();
        modifyContainer2.paxSurnameName2.sendKeys(paxsurname2);


        modifyContainer2.continuee.click();
    }
}
