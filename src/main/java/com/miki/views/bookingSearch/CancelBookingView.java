package com.miki.views.bookingSearch;

import com.miki.containers.bookingSearch.CancelBookingContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 09/01/15.
 */
public class CancelBookingView {


    private static final CancelBookingContainer cancelbookingContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), CancelBookingContainer.class);
    private static final Logger LOGGER = Logger.getLogger(CancelBookingView.class.getName());

    public static boolean isDisplayedCheck() {

        LOGGER.info(cancelbookingContainer.PageTitle);
        if (BrowserDriver.getPageTitle().trim().equalsIgnoreCase(cancelbookingContainer.PageTitle)) {

            return true;
        } else {
            LOGGER.info("Not in a SearchBooking Page. Error");
            return false;
        }
    }


    public static void cancelBookingItem() {

//        cancelbookingContainer.cancelBookingItemHeader.getText();

        cancelbookingContainer.termsConditionCheckBox.click();
        cancelbookingContainer.cancelButton.click();
    }
}
