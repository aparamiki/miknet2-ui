package com.miki.views.bookingSearch;

import com.miki.containers.bookingSearch.BookingSearchResultContainer;
import com.miki.utils.BrowserDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

/**
 * Created by asharma on 28/07/14.
 */
public class BookingSearchResult {


    private static final BookingSearchResultContainer searchBookingResult = PageFactory.initElements(BrowserDriver.getCurrentDriver(), BookingSearchResultContainer.class);
    private static final Logger LOGGER = Logger.getLogger(BookingSearchResult.class.getName());

    public static boolean isDisplayedCheck() {
        LOGGER.info("Booking Search Result Page");
        // check if page loaded successfully. Title is displayed
        if (BrowserDriver.getPageTitle().trim().equalsIgnoreCase(searchBookingResult.PageTitle)) {
            return true;
        } else {
            LOGGER.info("Not BookingSearchResult Page. Error");
            return false;
        }
    }


    public static void getBookingValues() {
        LOGGER.info("clientName " + searchBookingResult.clientName.getText());
        LOGGER.info("bookingDate " + searchBookingResult.bookingDate.getText());
        LOGGER.info("mikiReference " + searchBookingResult.mikiReference.getText());
        LOGGER.info("productName " + searchBookingResult.productName.getText());
        LOGGER.info(searchBookingResult.serviceDate.getText());
        LOGGER.info(searchBookingResult.bookingStatus.getText());
        LOGGER.info(searchBookingResult.subTotalPrice.getText());
        LOGGER.info(searchBookingResult.totalPrice.getText());
    }


    public static String getClientName() {
        return searchBookingResult.clientName.getText();
    }

    public static String bookingDate() {
        return searchBookingResult.bookingDate.getText();
    }


    public static String getServiceDate() {
        return searchBookingResult.serviceDate.getText();
    }

    public static String mikiReference() {
        return searchBookingResult.mikiReference.getText();
    }


    public static String getProductName() {
        return searchBookingResult.productName.getText();
    }

    public static String bookingStatus() {
        return searchBookingResult.bookingStatus.getText();
    }


    public static String subTotalPrice() {
        return searchBookingResult.subTotalPrice.getText();
    }

    public static String totalPrice() {
        return searchBookingResult.totalPrice.getText();
    }

    public static void cancelBooking()
    {
        searchBookingResult.cancelBookingLink.click();
    }


    /**
     * Click on Modify Booking Link
     */
    public static void modifyBooking()
    {
        searchBookingResult.modifyBookingLink.click();
    }


    public static void clickLogout() {
        searchBookingResult.logout.click();
    }

}
