package com.mikinet.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "json:target//cucumber.json"},
		features = "target//test-classes/features/", //path to the features
		glue = {"com.mikinet.steps"})
public class RunAllFeaturesTest {
}
