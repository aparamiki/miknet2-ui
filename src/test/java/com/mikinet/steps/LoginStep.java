package com.mikinet.steps;

import com.miki.mikinet.NavigationFactory;
import com.miki.mikinet.User;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by asharma on 12/06/14.
 */
public class LoginStep {

    public static final Logger LOGGER = Logger.getLogger(LoginStep.class.getName());

    private List<User> actualUsers = new ArrayList<User>();


    @Given("^I navigate to the mikinet application$")
    public void I_navigate_to_the_mikinet_application() {
        NavigationFactory.getNavigator().given_I_navigate_to_mikinet_application();
    }


    @When("^I try to login with :$")
    public void I_try_to_login_with_(DataTable arg1) throws Throwable {
        // For automatic conversion, change DataTable to List<YourType>
        actualUsers = arg1.asList(User.class);
        NavigationFactory.getNavigator().when_I_try_to_login(actualUsers);
    }


    @Then("^I should see that I logged in to HomePage$")
    public void I_should_see_that_I_logged_in_to_HomePage() {

    }


    @Then("^I should see the HomePage with \"([^\"]*)\"$")
    public void I_should_see_the_HomePage_with(String arg1) {

        if (NavigationFactory.getNavigator().getLoggedInUserName().equals(arg1.trim())) {
            Assert.assertTrue("User Logged in", true);
        } else {

           // Assert.assertTrue("User not logged in", false);
            NavigationFactory.getNavigator().Then_I_should_logout_by_clicking_on_logout_link();
            Assert.fail("Failing the tests");

        }
    }
//        Assert.assertEquals(NavigationFactory.getNavigator().then_I_should_see_the_homepage(), arg1.trim());


    @Then("^Then I should logout by clicking on logout link$")
    public void Then_I_should_logout_by_clicking_on_logout_link() {
        NavigationFactory.getNavigator().Then_I_should_logout_by_clicking_on_logout_link();

    }

//    @After
//    public void tearDown(Scenario scenario) {
//        if (scenario.isFailed()) {
//            // Take a screenshot...
//            final byte[] screenshot = ((TakesScreenshot) BrowserDriver.getCurrentDriver()).getScreenshotAs(OutputType.BYTES);
//            scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
//        }
//    }


}
