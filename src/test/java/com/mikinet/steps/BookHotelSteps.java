package com.mikinet.steps;

import com.miki.mikinet.NavigationFactory;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.AssertionFailedError;
import org.junit.Assert;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by asharma on 04/07/14.
 */
public class BookHotelSteps {

    public static final Logger LOGGER = Logger.getLogger(BookHotelSteps.class.getName());


    public boolean bookingIsModifiable;

    @Given("^I have logged into MikiNet Application successfully$")
    public void I_have_logged_into_MikiNet_Application_successfully() throws Throwable {
        Assert.assertTrue(NavigationFactory.getNavigator().given_I_navigate_to_mikinet_application());
        NavigationFactory.getNavigator().when_I_try_to_login("MTL001", "testadmin", "test1234");
    }


    @Given("^I am at  HomePage and select Book Hotel Option$")
    public void I_am_at_HomePage_and_select_Book_Hotel_Option() throws Throwable {
        Assert.assertTrue(NavigationFactory.getNavigator().then_I_should_see_the_homepage());
        NavigationFactory.getNavigator().I_click_on_Book_and_select_Hotels();
    }


    @When("^I search Search for Hotels with :$")
    public void I_search_Search_for_Hotels_with_(DataTable arg1) throws Exception {

        String pax = arg1.getGherkinRows().get(1).getCells().get(0);
        String city = arg1.getGherkinRows().get(1).getCells().get(1);
        String geolocation = arg1.getGherkinRows().get(1).getCells().get(2);
        String hotel = arg1.getGherkinRows().get(1).getCells().get(3);
        String currency = arg1.getGherkinRows().get(1).getCells().get(6);
        try {
//            Assert.assertTrue(NavigationFactory.getNavigator().When_I_Search_For_Hotel(pax, city, geolocation, hotel, format1.format(cal.getTime()), currency));
            Assert.assertTrue(NavigationFactory.getNavigator().When_I_Search_For_Hotel(pax, city, geolocation, hotel, null, currency));
        } catch (IOException e) {
            throw new AssertionFailedError(e.getMessage());
        }
    }


    @When("^I select miki contracted hotel  from the list$")
    public void I_select_miki_contracted_hotel_from_the_list() {
        NavigationFactory.getNavigator().When_I_Select_Hotel_From_List();
    }

    @When("^I check the availability of the hotel$")
    public void I_check_the_availability_of_the_hotel() {
        NavigationFactory.getNavigator().check_availability_of_hotel();

    }

    @When("^I book the hotel$")
    public void I_book_the_hotel() {
        NavigationFactory.getNavigator().bookingbasket_complete_booking();
    }

    @Then("^I booking should go in booking basket$")
    public void I_booking_should_go_in_booking_basket() {
        NavigationFactory.getNavigator().enterBookingReference();

    }

    @Then("^i should be successfully able to book the hotel$")
    public void i_should_be_successfully_able_to_book_the_hotel() {
        NavigationFactory.getNavigator().completeBooking();
        NavigationFactory.getNavigator().captureBookingDetails();
    }


    @Given("^I select client to book miki contracted hotels with :$")
    public void I_select_client_to_book_miki_contracted_hotels_with_(DataTable arg1) {
        NavigationFactory.getNavigator().Then_I_Should_Client_Page(arg1.getGherkinRows().get(1).getCells().get(0), arg1.getGherkinRows().get(1).getCells().get(1), arg1.getGherkinRows().get(1).getCells().get(2));
    }


    @Then("^I should logout of the mikinet.$")
    public void I_should_logout_of_the_mikinet() {
        NavigationFactory.getNavigator().Then_I_should_logout_by_clicking_on_logout_link_at_bookingcompletepage();
    }

    /**
     * Start :   Scenario Outline: Search & Cancel Miki Contracted Hotel
     * Modify booking
     */
    @When("^I read the Booking list from previously created booking with :$")
    public void I_read_the_Booking_list_from_previously_created_booking_with_(DataTable arg1) {
        String objectindex = arg1.getGherkinRows().get(1).getCells().get(0);
        NavigationFactory.getNavigator().then_I_should_see_the_homepage();
        NavigationFactory.getNavigator().I_click_on_Booking_and_select_SearchBooking(Integer.parseInt(objectindex));
    }

    @Then("^I should see the hotels booking detail$")
    public void I_should_see_the_hotels_booking_detail() {
        NavigationFactory.getNavigator().I_should_see_the_hotel_booking_details();
    }

    @Then("^I should be able to cancel the booking and logout$")
    public void i_should_be_able_to_cancel_the_booking_and_logout() throws Throwable {
        Assert.assertTrue(NavigationFactory.getNavigator().I_should_be_able_cancel_booking());
        Assert.assertTrue(NavigationFactory.getNavigator().I_should_also_see_cancallation_Confirmed_page());
    }

    /**
     * Created by asharma on 04/07/14.
     * Modify Booking Specific Steps
     * covering backGround steps
     */
    @Given("^Given I am at  HomePage and select Book Hotel Option$")
    public void given_I_am_at_HomePage_and_select_Book_Hotel_Option() throws Throwable {
        Assert.assertTrue(NavigationFactory.getNavigator().then_I_should_see_the_homepage());
        NavigationFactory.getNavigator().I_click_on_Book_and_select_Hotels();
    }


    @Given("^I am at mikinet url \"(.*?)\"$")
    public void i_am_at_mikinet_url(String arg1) throws Throwable {
        Assert.assertTrue(NavigationFactory.getNavigator().given_I_navigate_to_mikinet_application(arg1));

    }

    @Given("^I have logged with \"(.*?)\", \"(.*?)\", \"(.*?)\"$")
    public void i_have_logged_with(String arg1, String arg2, String arg3) {
        NavigationFactory.getNavigator().when_I_try_to_login(arg1, arg2, arg3);
    }


    @When("^Booking status is \"(.*?)\"$")
    public void booking_status_is(String arg1) {
        if (!(NavigationFactory.getNavigator().check_booking_status_to_modify(arg1))) {
            bookingIsModifiable = false;
        } else
            bookingIsModifiable = true;
    }

    @When("^I modify the booking by changing number of Nights and Name$")
    public void i_modify_the_booking_by_changing_number_of_Nights_and_Name(DataTable arg1) throws Throwable {

        if (bookingIsModifiable) {
            String nights = arg1.getGherkinRows().get(1).getCells().get(0);
            String paxtitle1 = arg1.getGherkinRows().get(1).getCells().get(1);
            String paxname1 = arg1.getGherkinRows().get(1).getCells().get(2);
            String paxsurname1 = arg1.getGherkinRows().get(1).getCells().get(3);

            String paxtitle2 = arg1.getGherkinRows().get(1).getCells().get(4);
            String paxname2 = arg1.getGherkinRows().get(1).getCells().get(5);
            String paxsurname2 = arg1.getGherkinRows().get(1).getCells().get(6);

            NavigationFactory.getNavigator().I_modify_the_booking_by_changing_number_of_Nights_and_Name(nights, paxtitle1, paxname1, paxsurname1, paxtitle2, paxname2, paxsurname2);
        } else {
            LOGGER.info("Not Modifiable");
        }
    }

    @Then("^I booking modification should be successful and logout$")
    public void i_booking_modification_should_be_successful_and_logout() {
        if (bookingIsModifiable) {
            NavigationFactory.getNavigator().completeModification();
            Assert.assertTrue(NavigationFactory.getNavigator().confirmModificationCompletion());
        }
        {
            LOGGER.info("Booking Not Modifiable");
        }
    }
}
