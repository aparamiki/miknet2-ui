@Functional
Feature: Hotel - Cancel and Book
  As a Client to MikiNet
  I want to be able to search the hotels
  I want to check the availability of the hotel
  I want to book the hotel
  I want to cancel te booking


  Scenario Outline: Book Miki Contracted Hotel
    Given   I am at mikinet url "https://test.mikinet.co.uk"
    Given I have logged with "MTL001", "testadmin", "test1234"
    Given I am at  HomePage and select Book Hotel Option
    Given I select client to book miki contracted hotels with :
      | Clientcode   | Clientname   | Username   |
      | <Clientcode> | <Clientname> | <Username> |
    When I search Search for Hotels with :
      | Pax   | City   | GeoLocation   | Hotel   | Date   | Number_of_nights   | Currency   |
      | <Pax> | <City> | <GeoLocation> | <Hotel> | <Date> | <Number_of_nights> | <Currency> |
    When  I select miki contracted hotel  from the list
    When I check the availability of the hotel
    When I book the hotel
    Then I booking should go in booking basket
    Then i should be successfully able to book the hotel
    Then I should logout of the mikinet.

  Examples:
    | Clientcode | Clientname              | Username             | Pax         | City   | GeoLocation            | Hotel | Date | Number_of_nights | Currency |
    | XXX001     | XXX001 (UFT TRAVEL LTD) | Apara Sharma (apara) | Switzerland | London | London, United Kingdom |       |      |                  | Eur      |


  Scenario Outline: Miki Supplier - Search & Cancel Booking
    Given   I am at mikinet url "https://test.mikinet.co.uk"
    Given I have logged with "MTL001", "testadmin", "test1234"
    When I read the Booking list from previously created booking with :
      | indexid   |
      | <indexid> |
    Then I should see the hotels booking detail
    Then I should be able to cancel the booking and logout

  Examples:
    | indexid |
    | 0       |


