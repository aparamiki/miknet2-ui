@Functional
Feature: MikiNet Test User  Login
  As a valid user
  I want to be able to login to MikiNet using Clinet Id, ClientCode and Password
  So that i can do various operation of Hotel search and booking


  Scenario Outline: Login Success and Failure
    Given   I am at mikinet url "https://test.mikinet.co.uk"
    When I try to login with :
      | Clientcode   | Username   | Password   |
      | <Clientcode> | <Username> | <Password> |
    Then I should see the HomePage with "<logged_in>"
    And  Then I should logout by clicking on logout link
  Examples:
    | Clientcode | Username  | Password | logged_in                                   |
    | MTL001     | testadmin | test1234 | Logged in as: Test Admin (MTL001 testadmin) |
    | MTL001     | kaveep    | kperera  | Logged in as: Kavee Perera (MTL001 kaveep)  |






