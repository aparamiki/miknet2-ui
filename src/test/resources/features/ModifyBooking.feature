@Functional
Feature: Hotel Book & Modify
  As a Client to MikiNet
  I want to modify  hotel bookings


  Scenario Outline: Create Booking

    Given I am at mikinet url "https://test.mikinet.co.uk"
    Given I have logged with "MTL001", "testadmin", "test1234"
    Given I am at  HomePage and select Book Hotel Option
    And I select client to book miki contracted hotels with :
      | Clientcode   | Clientname   | Username   |
      | <Clientcode> | <Clientname> | <Username> |
    When I search Search for Hotels with :
      | Pax   | City   | GeoLocation   | Hotel   | Date   | Number_of_nights   | Currency   |
      | <Pax> | <City> | <GeoLocation> | <Hotel> | <Date> | <Number_of_nights> | <Currency> |
    And  I select miki contracted hotel  from the list
    And I check the availability of the hotel
    And I book the hotel
    Then I booking should go in booking basket
    And i should be successfully able to book the hotel
    And I should logout of the mikinet.
  Examples:
    | Clientcode | Clientname              | Username             | Pax         | City   | GeoLocation            | Hotel | Date | Number_of_nights | Currency |
    | XXX001     | XXX001 (UFT TRAVEL LTD) | Apara Sharma (apara) | Switzerland | London | London, United Kingdom |       |      | 2                | Eur      |




#  OnRequest Booking 2474647
#  If booking is OnRequest then do not modify booking
  @ModifyHotel
  Scenario Outline: MOD - Reprice - Increase Night, Update Name, TWIN ROOM.
    Given   I am at mikinet url "https://test.mikinet.co.uk"
    Given I have logged with "MTL001", "testadmin", "test1234"
    When I read the Booking list from previously created booking with :
      | indexid   |
      | <indexid> |
    And Booking status is "Confirmed"
    And I modify the booking by changing number of Nights and Name
      | nights   | paxtitle1   | paxname1   | paxsurname1   | paxtitle2   | paxname2   | paxsurname2   |
      | <nights> | <paxtitle1> | <paxname1> | <paxsurname1> | <paxtitle2> | <paxname2> | <paxsurname2> |
    Then I booking modification should be successful and logout

  Examples:
    | indexid | nights | paxtitle1 | paxname1 | paxsurname1 | paxtitle2     | paxname2 | paxsurname2 |
    | 0       | 2      | Ms        | AparaMod | SharmaMod   | Not specified | AMod     | BMod        |

